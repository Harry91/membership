package stores_test

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"github.com/google/uuid"
	"go.appointy.com/google/pb/divisions"
	"go.appointy.com/google/pb/membership"

	"go.appointy.com/google/membership/internal/stores"
)

// RunMembershipStoreTests tests the membershipStore implementations for completeness
// Any Implementation can be passed to be tested for compliance
func RunMembershipStoreTests(s stores.MembershipStore, t *testing.T) {
	defer func() {
		if err := recover(); err != nil {
			t.Error(err)
		}
	}()

	mockMem := getMockMembership()
	mockMemId := getMockMemIdentifier()

	t.Run("Add", func(t *testing.T) {
		t.Run("Full", func(t *testing.T) {
			_, err := s.AddMembership(context.Background(), mockMem)
			if err != nil {
				t.Errorf("error while adding full object into store: %v", err)
			}
		})

		t.Run("Multiple Of Division", func(t *testing.T) {
			mockMem := getMockMembership()
			mockMem.Base = &divisions.DivisionRoot{
				ProgramId:  uuid.New().String(),
				LocationId: uuid.New().String(),
				DivisionId: uuid.New().String(),
			}

			for i := 1; i <= 5; i++ {
				t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
					_, err := s.AddMembership(context.Background(), mockMem)
					if err != nil {
						t.Errorf("error while adding memberships in parallel: %v", err)
					}
				})
			}
		})

		t.Run("Get", func(t *testing.T) {
			t.Run("One", func(t *testing.T) {
				gotMockMem, err := s.GetMembership(context.Background(), &membership.MembershipIndentifier{Base: &divisions.DivisionRoot{
					ProgramId:  mockMem.Base.ProgramId,
					LocationId: mockMem.Base.LocationId,
					DivisionId: mockMem.Base.DivisionId},
					MembershipId: mockMem.Id})
				if err != nil {
					t.Errorf("error getting value from identifier: %v", err)
				}

				if !reflect.DeepEqual(gotMockMem, mockMem) {
					t.Errorf("added Membership and returned Membership are different, added: %v, got: %v", mockMem, gotMockMem)
				}
			})

			t.Run("Of Division", func(t *testing.T) {
				_, err := s.GetAllMembership(context.Background())
				if err != nil {
					t.Errorf("could not get any Memberships: %v", err)
				}

				// if len(lcs) != insertCount {
				// 	t.Errorf("insert count: %d, not matched by get count: %d", insertCount, len(lcs))
				// }
			})
		})
	})

	t.Run("Update", func(t *testing.T) {
		updatedMockMem := getMockMembership()

		t.Run("One", func(t *testing.T) {
			updatedMockMem.Id = mockMem.Id

			err := s.UpdateMembership(context.Background(), updatedMockMem)
			if err != nil {
				t.Errorf("unable to update document: %v", err)
			}
		})

		t.Run("Get", func(t *testing.T) {
			mockMemId.MembershipId = mockMem.Id
			gotMockMem, err := s.GetMembership(context.Background(), mockMemId)
			if err != nil {
				t.Errorf("error getting value from id: %v", err)
			}

			if !reflect.DeepEqual(gotMockMem, updatedMockMem) {
				t.Errorf("updated Membership and returned Membership are different, updated: %v, got: %v", updatedMockMem, gotMockMem)
			}
		})
	})

	t.Run("Delete", func(t *testing.T) {
		t.Run("One", func(t *testing.T) {
			if err := s.DeleteMembership(context.Background(), mockMemId); err != nil {
				t.Errorf("error deleting Membership: %v", err)
			}
		})

		t.Run("Get", func(t *testing.T) {
			_, err := s.GetMembership(context.Background(), mockMemId)
			if err != stores.ErrNotFound {
				t.Errorf("not returning proper value upon not finding Membership: %v", err)
			}
		})
	})

	t.Run("Cancelled", func(t *testing.T) {
		t.Run("Add", func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			cancel()

			mockMem := getMockMembership()

			_, err := s.AddMembership(ctx, mockMem)
			if err == nil {
				t.Errorf("should return context.Cancelled but got: %v", err)
			}
		})

		t.Run("Get", func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			cancel()

			_, err := s.GetMembership(ctx, mockMemId)
			if err == nil {
				t.Errorf("should return context.Cancelled but got: %v", err)
			}
		})

		t.Run("GetAll", func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			cancel()

			_, err := s.GetAllMembership(ctx)
			if err == nil {
				t.Errorf("should return context.Cancelled but got: %v", err)
			}
		})

		t.Run("Delete", func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			cancel()

			err := s.DeleteMembership(ctx, mockMemId)
			if err == nil {
				t.Errorf("should return context.Cancelled but got: %v", err)
			}
		})

		t.Run("Update", func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			cancel()

			err := s.UpdateMembership(ctx, mockMem)
			if err == nil {
				t.Errorf("should return context.Cancelled but got: %v", err)
			}
		})
	})
}

func getMockMembership() *membership.Membership {
	return &membership.Membership{
		Base: &divisions.DivisionRoot{
			ProgramId:  uuid.New().String(),
			LocationId: uuid.New().String(),
			DivisionId: uuid.New().String(),
		},
		Id:             uuid.New().String(),
		Name:           "Harry",
		Description:    "Acrobat Training",
		MonthlyPrice:   450,
		ClassesAllowed: []int32{2, 5, 7},
	}
}

func getMockMemIdentifier() *membership.MembershipIndentifier {
	return &membership.MembershipIndentifier{
		Base: &divisions.DivisionRoot{
			ProgramId:  uuid.New().String(),
			LocationId: uuid.New().String(),
			DivisionId: uuid.New().String(),
		},
		MembershipId: uuid.New().String(),
	}
}
