package stores_test

import (
	"database/sql"
	"fmt"
	"testing"

	_ "github.com/lib/pq"
	"go.appointy.com/google/membership/internal/stores"
)

const (
	DB_HOST     = "10.0.0.77"
	DB_PORT     = 5432
	DB_USER     = "postgres"
	DB_PASSWORD = "Manhattan01919"
	DB_NAME     = "test"
)

func TestPostgresMembershipStore(t *testing.T) {

	conn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", conn)
	if err != nil {
		t.Fatal(err)
	}

	defer db.Close()

	// ping request to check connection with database.
	if err := db.Ping(); err != nil {
		t.Fatal(err)
	}

	s := stores.NewPostgresMembershipStore(db)

	RunMembershipStoreTests(s, t)

	// to delete ***all files*** from the table after test is completed.
	const delete_all_query = `DELETE FROM memberships."Memberships";`
	_, err = db.Exec(delete_all_query)
	if err != nil {
		t.Fatal(err)
	}
}
