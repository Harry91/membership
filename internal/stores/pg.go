package stores

import (
	"context"
	"database/sql"

	"github.com/lib/pq"

	"go.appointy.com/google/pb/divisions"

	"go.appointy.com/google/pb/membership"
)

// pg is the MembershipStore Implementation using the Postgres as its Backend
type pg struct {
	*sql.DB
}

// NewPostgresMembershipStore creates a new Membership Store with Postgres as its backend
func NewPostgresMembershipStore(db *sql.DB) MembershipStore {
	return &pg{db}
}

func (db *pg) AddMembership(ctx context.Context, m *membership.Membership) (*membership.MembershipIndentifier, error) {

	const query = `INSERT INTO memberships."Memberships" VALUES($1, $2, $3, $4, $5, $6, $7, $8);`
	_, err := db.ExecContext(ctx, query, m.Base.ProgramId, m.Base.LocationId, m.Base.DivisionId, m.Id, m.Name, m.Description, m.MonthlyPrice, pq.Array(m.ClassesAllowed))
	if err != nil {
		return nil, err
	}

	return &membership.MembershipIndentifier{
		Base: &divisions.DivisionRoot{
			ProgramId:  m.Base.ProgramId,
			LocationId: m.Base.LocationId,
			DivisionId: m.Base.DivisionId,
		},
		MembershipId: m.Id,
	}, nil
}

func (db *pg) UpdateMembership(ctx context.Context, m *membership.Membership) error {

	const query = `UPDATE memberships."Memberships" SET "ProgramId"=$1, "LocationId"=$2, "DivisionId"=$3, "Id"=$4, "Name"=$5, "Description"=$6, "MonthlyPrice"=$7, "ClassesAllowed"=$8;`
	_, err := db.ExecContext(ctx, query, m.Base.ProgramId, m.Base.LocationId, m.Base.DivisionId, m.Id, m.Name, m.Description, m.MonthlyPrice, pq.Array(m.ClassesAllowed))
	if err != nil {
		return err
	}
	return nil
}

func (db *pg) GetMembership(ctx context.Context, m *membership.MembershipIndentifier) (*membership.Membership, error) {

	mem := &membership.Membership{
		Base:           &divisions.DivisionRoot{},
		ClassesAllowed: []int32{},
	}

	const query = `SELECT * FROM memberships."Memberships" WHERE "ProgramId"=$1 AND "LocationId"=$2 AND "DivisionId"=$3 AND "Id"=$4;`
	err := db.QueryRowContext(ctx, query, m.Base.ProgramId, m.Base.LocationId, m.Base.DivisionId, m.MembershipId).Scan(&mem.Base.ProgramId, &mem.Base.LocationId, &mem.Base.DivisionId, &mem.Id, &mem.Name, &mem.Description, &mem.MonthlyPrice, pq.Array(&mem.ClassesAllowed))
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, err
	}
	return mem, nil
}

func (db *pg) DeleteMembership(ctx context.Context, m *membership.MembershipIndentifier) error {

	const query = `DELETE FROM memberships."Memberships" WHERE "ProgramId"=$1 AND "LocationId"=$2 AND "DivisionId"=$3 AND "Id"=$4;`
	_, err := db.ExecContext(ctx, query, m.Base.ProgramId, m.Base.LocationId, m.Base.DivisionId, m.MembershipId)
	if err != nil {
		if err == sql.ErrNoRows {
			return ErrNotFound
		}
		return err
	}

	return nil
}

func (db *pg) GetAllMembership(ctx context.Context) ([]*membership.Membership, error) {
	const query = `SELECT * FROM memberships."Memberships";`
	rows, err := db.QueryContext(ctx, query)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, err
	}

	members := make([]*membership.Membership, 0)

	for rows.Next() {

		mem := &membership.Membership{
			Base:           &divisions.DivisionRoot{},
			ClassesAllowed: []int32{},
		}

		err = rows.Scan(&mem.Base.ProgramId, &mem.Base.LocationId, &mem.Base.DivisionId, &mem.Id, &mem.Name, &mem.Description, &mem.MonthlyPrice, pq.Array(&mem.ClassesAllowed))
		if err != nil {
			return nil, err
		}

		members = append(members, mem)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return members, nil
}
