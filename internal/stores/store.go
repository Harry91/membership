package stores

import (
	"context"
	"errors"

	"go.appointy.com/google/pb/membership"
)

var (
	// ErrNotFound is the error returned by the store when the Membership Aggregate with given ID is not found
	ErrNotFound = errors.New("membership not found")
)

// MembershipStore is the storage abstraction required by the Membership Service to store and retreive data. It is the Aggregate Repository.
// It has been named store because of its seemingly NoSQL nature.
type MembershipStore interface {
	// AddMembership adds a Membership to the store and returns the newly assigned ID
	AddMembership(ctx context.Context, m *membership.Membership) (*membership.MembershipIndentifier, error)

	// UpdateMembership updates an existing Membership, if not found returns ErrNotFound
	UpdateMembership(ctx context.Context, m *membership.Membership) error

	// GetMembership retrieves the Membership Aggregate with the given ID
	GetMembership(ctx context.Context, m *membership.MembershipIndentifier) (*membership.Membership, error)

	// DeleteMembership removes the Membership Aggregate with the given Aggregate ID
	DeleteMembership(ctx context.Context, m *membership.MembershipIndentifier) error

	//GetAllMemberships gets all the memberships in the store
	GetAllMembership(ctx context.Context) ([]*membership.Membership, error)
}
