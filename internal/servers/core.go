package servers

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.appointy.com/google/membership/internal/stores"
	"go.appointy.com/google/pb/divisions"
	"go.appointy.com/google/pb/membership"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	errInternal = status.Error(codes.Internal, "internal server error") // Generic Error to be returned to client to hide possible sensitive information
)

// CoreServer is the business logic layer of the LocationsServer
type CoreServer struct {
	store  stores.MembershipStore
	divCli divisions.DivisionsClient
}

// NewCoreMembershipServer returns a LocationsServer implementation with Core business logic
func NewCoreMembershipServer(s stores.MembershipStore, c divisions.DivisionsClient) membership.MembershipsServer {
	return &CoreServer{
		store:  s,
		divCli: c,
	}
}

//AddMembership ...
func (s *CoreServer) AddMembership(ctx context.Context, m *membership.Membership) (*membership.MembershipIndentifier, error) {
	// Checking whether division exists or not.
	_, err := s.divCli.GetDivision(ctx, &divisions.DivisionIdentifier{Id: m.Base.DivisionId})
	if err != nil {
		return nil, err
	}

	if err := ValidateMembership(m); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Add Membership to the store
	mem, err := s.store.AddMembership(ctx, m)
	if err != nil {
		ctxzap.Extract(ctx).Error("unable to add membership to store", zap.Error(err))
		return nil, errInternal
	}
	return mem, nil
}

//GetAllMembership ...
func (s *CoreServer) GetAllMembership(ctx context.Context, e *empty.Empty) (*membership.MembershipList, error) {
	list, err := s.store.GetAllMembership(ctx)
	if err != nil {
		if err == stores.ErrNotFound {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, errInternal
	}

	return &membership.MembershipList{Memberships: list}, nil
}

//GetMembership ...
func (s *CoreServer) GetMembership(ctx context.Context, m *membership.MembershipIndentifier) (*membership.Membership, error) {
	mem, err := s.store.GetMembership(ctx, m)
	if err != nil {
		if err == stores.ErrNotFound {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		return nil, errInternal
	}
	return mem, nil
}

//UpdateMembership ...
func (s *CoreServer) UpdateMembership(ctx context.Context, m *membership.Membership) (*empty.Empty, error) {
	// Try to get membership
	if _, err := s.store.GetMembership(ctx, &membership.MembershipIndentifier{
		Base: &divisions.DivisionRoot{
			ProgramId:  m.Base.ProgramId,
			LocationId: m.Base.LocationId,
			DivisionId: m.Base.DivisionId,
		},
		MembershipId: m.Id,
	}); err != nil {
		return nil, err
	}

	if err := ValidateMembership(m); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// Try to update membership
	if err := s.store.UpdateMembership(ctx, m); err != nil {
		ctxzap.Extract(ctx).Error("unable to save membership to store", zap.Error(err))
		return nil, errInternal
	}

	return &empty.Empty{}, nil
}

//DeleteMembership ...
func (s *CoreServer) DeleteMembership(ctx context.Context, m *membership.MembershipIndentifier) (*empty.Empty, error) {
	// Try to fetch membership before deleting
	if _, err := s.store.GetMembership(ctx, m); err != nil {
		return nil, err
	}

	// Try to delete membership
	if err := s.store.DeleteMembership(ctx, m); err != nil {
		ctxzap.Extract(ctx).Error("unable to delete membership from store", zap.Error(err), zap.String("MembershipId", m.MembershipId)) // Log Unknown Error but return generic error to client
		return nil, errInternal
	}

	return &empty.Empty{}, nil
}
