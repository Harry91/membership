package servers_test

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"github.com/golang/protobuf/ptypes/empty"

	"go.appointy.com/google/pb/divisions"
	"go.appointy.com/google/pb/membership"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"go.appointy.com/google/membership/internal/servers"
	"go.appointy.com/google/membership/internal/servers/mocks"
	"go.appointy.com/google/membership/internal/stores"
	"go.appointy.com/google/membership/internal/stores/mocks"
)

func getMockMembership() *membership.Membership {
	return &membership.Membership{
		Base: &divisions.DivisionRoot{
			ProgramId:  uuid.New().String(),
			LocationId: uuid.New().String(),
			DivisionId: uuid.New().String(),
		},
		Id:             uuid.New().String(),
		Name:           "Harry",
		Description:    "FitnessService",
		MonthlyPrice:   float64(530),
		ClassesAllowed: []int32{2, 5, 7},
	}
}

func getMocks(t *testing.T) (*gomock.Controller, membership.MembershipsServer, *division_mocks.MockDivisionsClient, *store_mocks.MockMembershipStore) {
	ctrl := gomock.NewController(t)

	divCli := division_mocks.NewMockDivisionsClient(ctrl)
	store := store_mocks.NewMockMembershipStore(ctrl)

	srv := servers.NewCoreMembershipServer(store, divCli)

	return ctrl, srv, divCli, store
}

func TestCoreServer_AddMembership(t *testing.T) {
	type args struct {
		ctx context.Context
		in  *membership.Membership
	}
	type wants struct {
		out     *membership.MembershipIndentifier
		wantErr bool
	}
	tests := []struct {
		name  string
		a     *args
		w     *wants
		setup func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants)
	}{
		{
			name: "All Right",
			a: &args{
				ctx: context.Background(),
				in:  getMockMembership(),
			},
			w: &wants{
				out:     &membership.MembershipIndentifier{},
				wantErr: false,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				fc := dm.EXPECT().
					GetDivision(gomock.Any(), &divisions.DivisionIdentifier{Id: a.in.Base.DivisionId}).
					Return(nil, nil).
					Times(1)

				w.out.MembershipId = uuid.New().String()

				sm.EXPECT().
					AddMembership(gomock.Any(), a.in).
					Return(w.out, nil).
					After(fc).
					Times(1)
			},
		},
		{
			name: "Invalid Data",
			a: &args{
				ctx: context.Background(),
				in:  getMockMembership(),
			},
			w: &wants{
				out:     &membership.MembershipIndentifier{},
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				a.in.Name = ""

				dm.EXPECT().
					GetDivision(
						gomock.Any(),
						&divisions.DivisionIdentifier{
							Id: a.in.Base.DivisionId,
						},
					).
					Return(nil, nil).
					Times(1)
			},
		},
		{
			name: "Division Error",
			a: &args{
				ctx: context.Background(),
				in:  getMockMembership(),
			},
			w: &wants{
				out:     &membership.MembershipIndentifier{},
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				dm.EXPECT().
					GetDivision(
						gomock.Any(),
						&divisions.DivisionIdentifier{
							Id: a.in.Base.DivisionId,
						},
					).
					Return(nil, errors.New("")).
					Times(1)
			},
		},
		{
			name: "Store Error",
			a: &args{
				ctx: context.Background(),
				in:  getMockMembership(),
			},
			w: &wants{
				out:     &membership.MembershipIndentifier{},
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				fc := dm.EXPECT().
					GetDivision(
						gomock.Any(),
						&divisions.DivisionIdentifier{
							Id: a.in.Base.DivisionId,
						},
					).
					Return(nil, nil).
					Times(1)

				sm.EXPECT().
					AddMembership(gomock.Any(), a.in).
					Return(nil, errors.New("")).
					After(fc).
					Times(1)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, s, dm, sm := getMocks(t)
			defer c.Finish()

			tt.setup(tt.a, dm, sm, tt.w)

			got, err := s.AddMembership(tt.a.ctx, tt.a.in)
			if (err != nil) != tt.w.wantErr {
				t.Errorf("CoreServer.AddMembership() error = %v, wantErr %v", err, tt.w.wantErr)
				return
			}
			if !tt.w.wantErr && !reflect.DeepEqual(got, tt.w.out) {
				t.Errorf("CoreServer.AddMembership() = %v, want %v", got, tt.w.out)
			}
		})
	}
}

func TestCoreServer_GetMembership(t *testing.T) {
	type args struct {
		ctx context.Context
		m   *membership.MembershipIndentifier
	}
	type wants struct {
		m       *membership.Membership
		wantErr bool
	}
	tests := []struct {
		name  string
		a     *args
		w     *wants
		setup func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants)
	}{
		{
			name: "Sucess",
			a: &args{
				ctx: context.Background(),
				m:   &membership.MembershipIndentifier{},
			},
			w: &wants{
				m:       getMockMembership(),
				wantErr: false,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				a.m.Base = w.m.Base
				// a.m.Base.LocationId = w.m.Base.LocationId
				// a.m.Base.DivisionId = w.m.Base.DivisionId
				a.m.MembershipId = w.m.Id

				sm.EXPECT().
					GetMembership(gomock.Any(), a.m).
					Return(w.m, nil).
					Times(1)
			},
		},

		{
			name: "ErrNotFound",
			a: &args{
				ctx: context.Background(),
				m:   &membership.MembershipIndentifier{},
			},
			w: &wants{
				m:       getMockMembership(),
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				a.m.Base = w.m.Base
				a.m.MembershipId = ""

				sm.EXPECT().
					GetMembership(gomock.Any(), a.m).
					Return(nil, stores.ErrNotFound).
					Times(1)
			},
		},

		{
			name: "Store Error",
			a: &args{
				ctx: context.Background(),
				m:   &membership.MembershipIndentifier{},
			},
			w: &wants{
				m:       getMockMembership(),
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				a.m.Base = w.m.Base
				a.m.MembershipId = ""

				sm.EXPECT().
					GetMembership(gomock.Any(), a.m).
					Return(nil, errors.New("Error")).
					Times(1)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, s, dm, sm := getMocks(t)
			defer c.Finish()

			tt.setup(tt.a, dm, sm, tt.w)

			got, err := s.GetMembership(tt.a.ctx, tt.a.m)
			if (err != nil) != tt.w.wantErr {
				t.Errorf("CoreServer.GetMembership() error = %v, wantErr %v", err, tt.w.wantErr)
				return
			}
			if !tt.w.wantErr && !reflect.DeepEqual(got, tt.w.m) {
				t.Errorf("CoreServer.GetMembership() = %v, want %v", got, tt.w.m)
			}
		})
	}
}

func TestCoreServer_UpdateMembership(t *testing.T) {
	type args struct {
		ctx context.Context
		m   *membership.Membership
	}
	type wants struct {
		wantErr bool
	}
	tests := []struct {
		name  string
		a     *args
		w     *wants
		setup func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants)
	}{
		{
			name: "Sucess",
			a: &args{
				ctx: context.Background(),
				m:   getMockMembership(),
			},
			w: &wants{
				wantErr: false,
			},
			setup: func(a *args, _ *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				a.m.Id = uuid.New().String()
				mockMemId := &membership.MembershipIndentifier{
					Base: &divisions.DivisionRoot{
						ProgramId:  a.m.Base.ProgramId,
						LocationId: a.m.Base.LocationId,
						DivisionId: a.m.Base.DivisionId,
					},
					MembershipId: a.m.Id,
				}

				getCall := sm.EXPECT().
					GetMembership(gomock.Any(), mockMemId).Return(a.m, nil).Times(1)

				sm.EXPECT().
					UpdateMembership(gomock.Any(), a.m).
					Return(nil).
					Times(1).
					After(getCall)
			},
		},

		{
			name: "Store Update Fail",
			a: &args{
				ctx: context.Background(),
				m:   getMockMembership(),
			},
			w: &wants{
				wantErr: true,
			},
			setup: func(a *args, _ *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				a.m.Id = uuid.New().String()
				mockInput := &membership.MembershipIndentifier{
					Base: &divisions.DivisionRoot{
						ProgramId:  a.m.Base.ProgramId,
						LocationId: a.m.Base.LocationId,
						DivisionId: a.m.Base.DivisionId,
					},
					MembershipId: a.m.Id,
				}

				getCall := sm.EXPECT().
					GetMembership(gomock.Any(), mockInput).Return(a.m, nil).Times(1)

				sm.EXPECT().
					UpdateMembership(gomock.Any(), a.m).
					Return(errors.New("")).
					Times(1).
					After(getCall)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, s, dm, sm := getMocks(t)
			defer c.Finish()

			tt.setup(tt.a, dm, sm, tt.w)

			_, err := s.UpdateMembership(tt.a.ctx, tt.a.m)
			if (err != nil) != tt.w.wantErr {
				t.Errorf("CoreServer.UpdateMembership() error = %v, wantErr %v", err, tt.w.wantErr)
				return
			}
		})
	}
}

func TestCoreServer_DeleteMembership(t *testing.T) {
	type args struct {
		ctx context.Context
		m   *membership.MembershipIndentifier
	}
	type wants struct {
		m       *empty.Empty
		wantErr bool
	}
	tests := []struct {
		name  string
		a     *args
		w     *wants
		setup func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants)
	}{
		{
			name: "Success",
			a: &args{
				ctx: context.Background(),
				m:   &membership.MembershipIndentifier{},
			},
			w: &wants{
				wantErr: false,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				temp := &membership.Membership{}
				temp.Base = a.m.Base
				temp.Id = a.m.MembershipId
				getCall := sm.EXPECT().
					GetMembership(gomock.Any(), a.m).
					Return(temp, nil).
					Times(1)
				sm.EXPECT().DeleteMembership(gomock.Any(), a.m).Return(nil).Times(1).After(getCall)
			},
		},

		{
			name: "Store Delete Error",
			a: &args{
				ctx: context.Background(),
				m:   &membership.MembershipIndentifier{},
			},
			w: &wants{
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {
				temp := &membership.Membership{}
				temp.Base = a.m.Base
				temp.Id = a.m.MembershipId

				getCall := sm.EXPECT().GetMembership(gomock.Any(), a.m).
					Return(temp, nil).Times(1)
				sm.EXPECT().DeleteMembership(gomock.Any(), a.m).Return(errors.New("")).Times(1).After(getCall)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, s, dm, sm := getMocks(t)
			defer c.Finish()

			tt.setup(tt.a, dm, sm, tt.w)

			_, err := s.DeleteMembership(tt.a.ctx, tt.a.m)
			if (err != nil) != tt.w.wantErr {
				t.Errorf("CoreServer.DeleteMembership() error = %v, wantErr %v", err, tt.w.wantErr)
				return
			}
		})
	}
}

func TestCoreServer_GetAllMembership(t *testing.T) {
	type args struct {
		ctx context.Context
		e   *empty.Empty
	}
	type wants struct {
		m       *membership.MembershipList
		wantErr bool
	}
	tests := []struct {
		name  string
		a     *args
		w     *wants
		setup func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants)
	}{
		{
			name: "Success",
			a: &args{
				ctx: context.Background(),
				e:   &empty.Empty{},
			},
			w: &wants{
				m:       &membership.MembershipList{},
				wantErr: false,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {

				w.m.Memberships = []*membership.Membership{}

				for i := 0; i < 5; i++ {
					m := getMockMembership()
					w.m.Memberships = append(w.m.Memberships, m)
				}

				sm.EXPECT().
					GetAllMembership(gomock.Any()).
					Return(w.m.Memberships, nil).
					Times(1)
			},
		},
		{
			name: "Store Error",
			a: &args{
				ctx: context.Background(),
				e:   &empty.Empty{},
			},
			w: &wants{
				m:       &membership.MembershipList{},
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {

				w.m.Memberships = []*membership.Membership{}

				for i := 0; i < 5; i++ {
					m := getMockMembership()
					w.m.Memberships = append(w.m.Memberships, m)
				}

				sm.EXPECT().
					GetAllMembership(gomock.Any()).
					Return(nil, errors.New("Error")).
					Times(1)
			},
		},

		{
			name: "ErrNotFound",
			a: &args{
				ctx: context.Background(),
				e:   &empty.Empty{},
			},
			w: &wants{
				m:       &membership.MembershipList{},
				wantErr: true,
			},
			setup: func(a *args, dm *division_mocks.MockDivisionsClient, sm *store_mocks.MockMembershipStore, w *wants) {

				w.m.Memberships = []*membership.Membership{}

				for i := 0; i < 5; i++ {
					m := getMockMembership()
					w.m.Memberships = append(w.m.Memberships, m)
				}

				sm.EXPECT().
					GetAllMembership(gomock.Any()).
					Return(nil, stores.ErrNotFound).
					Times(1)
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, s, dm, sm := getMocks(t)
			defer c.Finish()

			tt.setup(tt.a, dm, sm, tt.w)

			got, err := s.GetAllMembership(tt.a.ctx, tt.a.e)
			if (err != nil) != tt.w.wantErr {
				t.Errorf("CoreServer.GetAllMembership() error = %v, wantErr %v", err, tt.w.wantErr)
				return
			}
			if !tt.w.wantErr && !reflect.DeepEqual(got, tt.w.m) {
				t.Errorf("CoreServer.GetAllMembership() = %v, want %v", got, tt.w.m)
			}
		})
	}
}
