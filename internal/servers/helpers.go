package servers

import (
	"errors"
	"strings"

	"go.appointy.com/google/pb/membership"
)

// ValidateMembership statically validates the Membership Object
func ValidateMembership(m *membership.Membership) error {

	// Validate ProgramId to see if its not empty
	if strings.TrimSpace(m.Base.ProgramId) == "" {
		return errors.New("ProgramId cannot be empty")
	}

	// Validate LocationId to see if its not empty
	if strings.TrimSpace(m.Base.LocationId) == "" {
		return errors.New("LocationId cannot be empty")
	}

	// Validate DivisionId to see if its not empty
	if strings.TrimSpace(m.Base.DivisionId) == "" {
		return errors.New("DivisionId cannot be empty")
	}

	//// Validate Id to see if its not empty
	if strings.TrimSpace(m.Id) == "" {
		return errors.New("Id cannot be empty")
	}

	// Validate Name to see if its not empty
	if strings.TrimSpace(m.Name) == "" {
		return errors.New("Name cannot be empty")
	}

	return nil
}
